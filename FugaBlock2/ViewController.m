//
//  ViewController.m
//  FugaBlock2
//
//  Created by pies on 2018/11/22.
//  Copyright © 2018年 pies. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (nonatomic) NSString *htmlPath;
@property (nonatomic) NSString *htmlSrc;
@property (weak, nonatomic) IBOutlet UIWebView *webview;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _htmlPath = [[NSBundle mainBundle] pathForResource:@"hoge" ofType:@"html"];
    _htmlSrc = [NSString stringWithContentsOfFile:_htmlPath encoding:NSUTF8StringEncoding error:nil];
    NSLog(@"path :%@",_htmlSrc);
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self loadWebView:_htmlSrc];
}


- (IBAction)tapNormaLoad:(id)sender {

    [self loadWebView:_htmlSrc];


}

- (IBAction)tapBlockEditLoad:(id)sender {
    
    // 編集処理を渡して表示
    [self loadWebView:_htmlSrc editProc:^(NSString* baseSrc) {
        baseSrc = [baseSrc stringByReplacingOccurrencesOfString:@"ほげー" withString:@"ふがー"];
        return baseSrc;
    }];
}

-(void)loadWebView:(NSString*)src
{
    // 編集処理を渡さない
    [self loadWebView:src editProc:nil];
}
-(void)loadWebView:(NSString*)src editProc:(NSString* (^)(NSString* baseSrc))block
{
    NSString *loadSrc = [src copy];
    if (block)
    {
        // 編集処理が渡された場合のみ編集を実行
        loadSrc = block(loadSrc);
    }
    [_webview loadHTMLString:loadSrc baseURL:[NSURL URLWithString:@"/"]];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
